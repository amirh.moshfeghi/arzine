from account.models import *

def update_wage(user):
    account = Account.objects.filter(user=user).first()

    btrans = Transactions.objects.filter(buyer=user)
    strans = Transactions.objects.filter(seller=user)
    c=0
    for i in btrans:
        c += float(i.fpu) * float(i.amount)
    for i in strans:
        c += float(i.fpu) * float(i.amount)
    if c < 20000000:
        account.wage = 0.4
    elif c < 50000000:
        account.wage = 0.35
    elif c < 100000000:
        account.wage = 0.3
    elif c > 100000000:
        account.wage = 0.2
    account.save()
