import requests
import json
from django.shortcuts import redirect
from django.http import HttpResponse,HttpResponseRedirect



api_key='b44fbd8273bd9988900a64c7b969593b5d004046'
callback_url = 'http://localhost:8000/account/verify/'


def Get_token(amount, mobile,user):
    url = "https://vandar.io/api/ipg/send"

    data = {}
    data['api_key'] = api_key
    data['amount'] = amount
    data['mobile_number'] = mobile
    data['description'] = user.id
    data['callback_url'] = callback_url
    json_data = json.dumps(data)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=json_data)
    return response.json()


def Transaction(token):
    url = "https://vandar.io/api/ipg/2step/transaction"

    data={}
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    data['api_key'] = api_key
    data['token'] = token
    json_data = json.dumps(data)
    response = requests.request("POST", url, headers=headers, data=json_data)
    return response.json()



def Verify(token):
    url = "https://ipg.vandar.io/api/v3/verify"

    data = {}
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    data['api_key'] = api_key
    data['token'] = token
    json_data = json.dumps(data)
    response = requests.request("POST", url, headers=headers, data=json_data)
    return response.json()
