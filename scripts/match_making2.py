from account.models import User, Account, Transactions2, Order2, BlockedCoin, Notification
import datetime
from constants import *
from constants.notifs import messagessss


def fix_order(order, acc):
    b = BlockedCoin.objects.filter(block=acc).first()

    if order.trans_type == 'b':
        if order.order_type == 'n':
            print("jooon")
            b.teth = float(b.teth) + float(order.amount) * float(order.fpu) * (1 + float(acc.wage) / 100)
            acc.teth = float(acc.teth) - float(order.amount) * float(order.fpu) * (1 + float(acc.wage) / 100)

            acc.save()
            b.save()



    elif order.trans_type == 's':

        setattr(b, order.cryp, float(getattr(b, order.cryp)) + float(order.amount))
        setattr(acc, order.cryp, float(getattr(acc, order.cryp)) - float(order.amount))

        if order.order_type == 'n':
            acc.teth = float(acc.teth) - float(order.amount) * float(order.fpu) * (float(acc.wage) / 100)
            b.teth = float(b.teth) + float(order.amount) * float(order.fpu) * (float(acc.wage) / 100)

        b.save()
        acc.save()

    order.description = 'pending to execute'
    order.save()
    notifs = Notification(order=order,user=order.user,title=(messagessss['order'] + str(order.fpu)))
    notifs.save()

    return order


def expire(today):
    expired = Order2.objects.filter(limited='y').filter(due_date__lt=today)
    for ex in expired:

        user = User.objects.filter(id=ex.user.id).first()
        acc = Account.objects.filter(user=user).first()
        b = BlockedCoin.objects.filter(block=acc).first()

        if ex.trans_type == 'b':
            if ex.order_type == 'n':
                acc.teth = float(acc.teth) + float(ex.remnant) * float(ex.fpu) * (1 + float(acc.wage) / 100)
                b.teth = float(b.teth) - float(ex.remnant) * float(ex.fpu) * (1 + float(acc.wage) / 100)

        else:
            setattr(b, ex.cryp, float(getattr(b, ex.cryp)) - float(ex.remnant))
            setattr(acc, ex.cryp, float(getattr(acc, ex.cryp)) + float(ex.remnant))

            if ex.order_type == 'n':
                acc.teth = float(acc.teth) + float(ex.remnant) * float(ex.fpu) * (float(acc.wage) / 100)
                b.teth = float(b.teth) - float(ex.remnant) * float(ex.fpu) * (float(acc.wage) / 100)

            acc.save()
            b.save()

        ex.description = 'expired'
        ex.remnant = 0
        ex.save()


def find_buyer(order, today):
    seller = User.objects.filter(id=order.user.id).first()

    if order.order_type == 'n':

        buy_orders = Order2.objects.filter(cryp=order.cryp).filter(trans_type='b').filter(fpu=order.fpu) \
                     | Order2.objects.filter(cryp=order.cryp).filter(trans_type='b').filter(order_type='f')

        buy_orders = buy_orders.filter(remnant__gt=0).order_by('time')

        for bo in buy_orders:

            if float(order.remnant) > 0:

                buyer = User.objects.filter(id=bo.user.id).first()
                trans_amount = min(float(order.remnant), float(bo.remnant))

                if (bo.order_type == 'f'):
                    b = Account.objects.filter(id=bo.user.id).first()
                    block = BlockedCoin.objects.filter(block=b).first()

                    if (float(trans_amount) * float(order.fpu) * (1 + float(b.wage) / 100) > float(b.teth)):
                        trans_amount = float(b.teth) / (float(order.fpu) * (1 + float(b.wage) / 100))

                    if trans_amount > 0:
                        block.teth = float(block.teth) + float(trans_amount) * float(order.fpu) * (
                                    1 + float(b.wage) / 100)
                        b.teth = float(b.teth) - float(trans_amount) * float(order.fpu) * (1 + float(b.wage) / 100)
                        b.save()

                if trans_amount > 0:
                    print(seller.id, buyer.id)
                    create_Transaction(seller, buyer, trans_amount, bo, order, today, order.fpu,seller)



    else:

        acc = Account.objects.filter(user=seller).first()
        block = BlockedCoin.objects.filter(block=acc).first()
        normal_buy_orders = Order2.objects.filter(cryp=order.cryp).filter(trans_type='b').filter(order_type='n').filter(
            remnant__gt=0).order_by('fpu')
        normal_buy_orders.reverse()

        for bo in normal_buy_orders:
            if float(order.remnant) > 0:
                buyer = User.objects.filter(id=bo.user.id).first()
                trans_amount = min(float(order.remnant), float(bo.remnant))
                if (float(acc.teth) >= float(trans_amount) * float(bo.fpu) * (float(acc.wage) / 100)):
                    acc.teth = float(acc.teth) - float(trans_amount) * float(bo.fpu) * (float(acc.wage) / 100)
                    block.teth = float(block.teth) + float(trans_amount) * float(bo.fpu) * (float(acc.wage) / 100)
                    create_Transaction(seller, buyer, trans_amount, bo, order, today, bo.fpu,seller)


def find_seller(order, today):
    buyer = User.objects.filter(id=order.user.id).first()

    if order.order_type == 'n':

        sell_orders = Order2.objects.filter(cryp=order.cryp).filter(trans_type='s').filter(fpu=order.fpu) \
                      | Order2.objects.filter(cryp=order.cryp).filter(trans_type='s').filter(order_type='f')

        sell_orders = sell_orders.filter(remnant__gt=0).order_by('time')

        for so in sell_orders:

            if float(order.remnant) > 0:
                seller = User.objects.filter(id=so.user.id).first()
                trans_amount = min(float(order.remnant), float(so.remnant))
                print(seller.id, buyer.id)
                create_Transaction(seller, buyer, trans_amount, order, so, today, order.fpu,buyer)


    else:

        normal_sell_orders = Order2.objects.filter(cryp=order.cryp).filter(trans_type='s').filter(order_type='n').filter(
            remnant__gt=0).order_by('fpu')

        print(normal_sell_orders)

        for so in normal_sell_orders:
            if float(order.remnant) > 0:
                seller = User.objects.filter(id=so.user.id).first()
                acc = Account.objects.filter(user=buyer).first()
                block = BlockedCoin.objects.filter(block=acc).first()

                trans_amount = min(float(order.remnant), float(so.remnant))

                if (float(trans_amount) * float(so.fpu) > float(acc.teth)):
                    trans_amount = float(acc.teth) / float(so.fpu)

                if trans_amount > 0:
                    print(float(trans_amount) * float(so.fpu) * (float(acc.wage) / 100))
                    setattr(block, 'teth', float(getattr(block, 'teth')) + float(trans_amount) * float(so.fpu) * (
                                1 + float(acc.wage) / 100))
                    setattr(acc, 'teth', float(getattr(acc, 'teth')) - float(trans_amount) * float(so.fpu) * (
                                1 + float(acc.wage) / 100))
                    block.save()
                    acc.save()

                if trans_amount > 0:
                    create_Transaction(seller, buyer, trans_amount, order, so, today, so.fpu)


def Transact(seller_account, buyer_account, money, amount, cryp, order):
    bs = BlockedCoin.objects.filter(block=seller_account).first()
    bb = BlockedCoin.objects.filter(block=buyer_account).first()

    print(float(money) * (1 + float(buyer_account.wage) / 100))
    setattr(bb, 'teth', float(getattr(bb, 'teth')) - float(money) * (1 + float(buyer_account.wage) / 100))
    setattr(buyer_account, cryp, float(getattr(buyer_account, cryp)) + float(amount))

    setattr(bs, cryp, float(getattr(bs, cryp)) - float(amount))
    setattr(bs, 'teth', float(getattr(bs, 'teth')) - float(money) * (float(seller_account.wage) / 100))
    seller_account.teth = float(seller_account.teth) + float(money)

    bb.save()
    bs.save()

    seller_account.save()
    buyer_account.save()


def order_alter(sorder, border, amount):
    sorder.remnant = float(sorder.remnant) - float(amount)
    border.remnant = float(border.remnant) - float(amount)

    if (float(sorder.remnant) == 0):
        sorder.description = 'done'

    if (float(border.remnant) == 0):
        border.description = 'done'

    border.save()
    sorder.save()


def create_Transaction(seller, buyer, amount, border, sorder, today, fpu,user):
    cryp = border.cryp
    money = float(amount) * float(fpu)
    seller_account = Account.objects.filter(user=seller).first()
    buyer_account = Account.objects.filter(user=buyer).first()

    Transact(seller_account, buyer_account, money, amount, cryp, border)
    order_alter(sorder, border, amount)

    f_address = getattr(seller_account, cryp + '_address')
    t_address = getattr(buyer_account, cryp + '_address')
    T = Transactions2(seller=seller, buyer=buyer, cryp=cryp, amount=amount, fpu=fpu,
                     date=today, from_address=f_address, to_address=t_address)

    T.save()
    notifs = Notification(trans=T, user=user, title=messagessss['trans'])
    notifs.save()


def match(order):
    today = str(datetime.datetime.now()).split()[0]
    # expire(today)

    if (order.trans_type) == 'b':
        find_seller(order, today)
    else:
        find_buyer(order, today)

