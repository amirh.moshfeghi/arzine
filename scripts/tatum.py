import http.client
import json
from account.models import User
from constants.hd_urls import urls


def set_address_eth(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    eth_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6EBUZfHWJb8PXfmEjDofah2wBCUeqX6NzMdXTsoiHVyYdN7xTywsENAPKteapsqbyf4DtNcQECDWsNApX3KHsYmU4Re8UgikvWnDkcXdEWr/'
    url_total=eth_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_xcon(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    xcon_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6EiMvdkHuohXSaGLuiFMZGAufRGnS4buvCuUNdHYo6N5SeM6qmfz4LWwvu1BMqbSLWKJGMjeXMuGsBboChSGWJVe2LgbrWCSAfXHHz5JaPH/'
    url_total=xcon_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_wbtc(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    wbtc_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6DwvoGqWLMuMZDu5Xj8wooikhe1SjbAgz9mWEqRuTjcvYhwUfmWy9Scv8wqJQFe7fJH6WhstXnPXimbH376UU8CprQvhjdUCsjyGZwndTMa/'
    url_total=wbtc_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_free(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    free_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6Exm7fiADCYNM92zbhbr82gtbf96gr2z3aRZknRTpCb4HVkcF8c4t37C9EtUNCkzjxq78YiZKaRN7cbwufkSYJaTfEakdrZB5ydfwmAxwuQ/'
    url_total=free_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_leo(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    leo_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6DcK3bXVEoDig6Xqb5HwCg1HqUhuuJ36c2w9iumj1XwHtR9GNwusTDQaxjsfd9g9zuwrSKQ9WgFQ73tvcfR2rEF4fZXQxH487mk18oAzdtP/'
    url_total=leo_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_link(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    link_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6DwB6DsZYDJgAVsCCAas6rRumhVeSZZA5ZvYLLn76Sp7VqWeRF7uEQxLirRrAf1VGJfWEgnnK4gRjqUX73YeJD6bUkgM1d9Z3AJrveUBqH7/'
    url_total=link_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_maker(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    maker_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6DZTuaSzNHNnmUs1aewjrt4c4nDoUxtsLVjiWRnSTSrMuutq7iMfmV33ZwhFhaVuDY4e1pN4CaqWZjEFENMNpkaFFrvNDVR3L3KeGD2AJCD/'
    url_total=maker_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_pax(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    pax_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6EU4iNNBR3B7QUYGwqYTUo7YY15kQBgsePxB3Fqa4Gct7shRbobTkAJEjUTKeW1w5nUJQwTGV6nJhnRohQJFCTmNPNc8arTD4in2rqNNnq3/'
    url_total=pax_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_paxg(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    paxg_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6EcroHWz1kWd9ThdV9XiFMho2m9m2bU5vpuF8H83d785gaTDPP6fz4njNfiiLh3RwAWNsgbWNutCWSictUgd71HfHMixUC3u6ndVmWE1XHX/'
    url_total=paxg_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_platon(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    platon_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6FDFtxFPEqzh97LYqqZexT5jAdW7vJQ41Fm5yQN342PDgaAdu5ys7UxMfUJdP3b7c72GBDdy8jSAqmKSfLFLvut3kW78rfYXXe3H7TYHDXx/'
    url_total=platon_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_tusd(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    tusd_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6FLM6ccFuaUpRViDue22DTAZfFhmoT5gMyHzUR2W8atx64fhqjtPBVSuoWVvEKNG1rAzcT6kveRqKGDFWNhkVVWP8jT3edPBVaEW5ok1Nqc/'
    url_total=tusd_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()
def set_address_uni(account , cryp,user_id):
    conn = http.client.HTTPSConnection("api-eu1.tatum.io")
    headers = {'x-api-key': "54bbf4eb-5bfb-41c1-b120-b8d526147c67"}
    uni_url='https://api-eu1.tatum.io/v3/ethereum/address/xpub6DsWSK4vR9fcx8whYVkXbu1FxUWb9hKqA1skSPcnx1UZXTxctsDw1TV9PGE6i71E3UGxNek7RYse1k946AJfGhdH1WgMufcf3XX5LKyWYMR/'
    url_total=uni_url+str(user_id)

    conn.request("GET",url_total,headers=headers)

    res = conn.getresponse()
    data = res.read()
    total = data.decode("utf-8")[12:-2]

    setattr(account, cryp+'_address',total)
    account.save()




