from django.shortcuts import render, get_object_or_404
from django.utils.text import slugify

from scripts.wage import update_wage
from .models import Profile as Ps, Notification

from django.contrib.auth.decorators import login_required
from django.db.models import Max, Min, Sum
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView, PasswordChangeView
from django.db.models import Max, Min
from scripts.match_making2 import fix_order, match
from .models import User, Account, Tickets, Order2, Transactions2, UserAuthModel, Bank_Account, BlockedCoin, P2P_Acc2, P2P2, \
    UserWallet
from constants.cryptos import cryptos
from .forms import ProfileForm, FastSellForm, ChargeForm, AuthForm, PhoneVerify, NormalSellForm, FastBuyForm, \
    NormalBuyForm, P2PCreateForm, WalletForm, Wverify
from django.contrib import messages
import random, string, datetime

from django.contrib.auth import update_session_auth_hash, logout
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from scripts.payment import Get_token, Transaction, Verify
from scripts.hd_wallet import set_address
from django.http import HttpResponseRedirect
from scripts.tatum import set_address_eth, set_address_leo, set_address_pax, set_address_paxg, set_address_uni, \
    set_address_link, set_address_platon, set_address_free, set_address_maker, set_address_wbtc, set_address_tusd, \
    set_address_xcon
import ghasedak
from random import randint

import json
from .mixins import (
    FieldsMixin,
    FormValidMixin,
    AuthorsAccessMixin,
    SuperUserAccessMixin
)
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DeleteView
)



import ghasedak




from django.http import HttpResponse
from .forms import SignupForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import EmailMessage


@login_required
def home2(request, *args, **kwargs):
    code = str(kwargs.get('ref_code'))
    try:
        prof = Ps.objects.get(code=code)
        request.session['ref_prof'] = prof.id
    except:
        pass

    user = User.objects.filter(id=request.user.id).first()
    p2ps = Transactions2.objects.filter(trans_type='p2p').filter(seller=user)
    user_p2p = P2P2.objects.filter(user=user)
    # for item in user_p2p:
    #     if Transactions.objects.filter(trans_type='p2p')
    rec = Ps.objects.filter(user=user)
    max_btc = Transactions2.objects.filter(cryp='btc').aggregate(Max('fpu'))
    max_eth = Transactions2.objects.filter(cryp='eth').aggregate(Max('fpu'))
    min_eth = Transactions2.objects.filter(cryp='eth').aggregate(Max('fpu'))
    min_btc = Transactions2.objects.filter(cryp='btc').aggregate(Min('fpu'))
    max_ltc = Transactions2.objects.filter(cryp='ltc').aggregate(Max('fpu'))
    min_ltc = Transactions2.objects.filter(cryp='ltc').aggregate(Min('fpu'))
    max_dash = Transactions2.objects.filter(cryp='dash').aggregate(Max('fpu'))
    min_dash = Transactions2.objects.filter(cryp='dash').aggregate(Min('fpu'))
    max_bch = Transactions2.objects.filter(cryp='bch').aggregate(Max('fpu'))
    min_bch = Transactions2.objects.filter(cryp='bch').aggregate(Min('fpu'))
    max_usdt = Transactions2.objects.filter(cryp='usdt').aggregate(Max('fpu'))
    max_usdt = Transactions2.objects.filter(cryp='usdt').aggregate(Min('fpu'))
    obj = Notification.objects.filter(user=user)
    # sum = Transactions.objects.filter(user=user).aggregate(Sum('amount'))
    sum = Transactions2.objects.all().aggregate(Sum('amount'))
    # tsum = Transactions.objects.filter(user=user).count()
    tsum = Transactions2.objects.all().count()
    get_sum = (sum.get('amount__sum'))
    if not get_sum:
        get_sum = 0
    acc = Account.objects.filter(user=user).first()
    blocked = BlockedCoin.objects.filter(block=acc).first()
    balance = {}
    balance['btc'] = acc.btc
    balance['eth'] = acc.eth
    balance['maker'] = acc.maker
    balance['uni'] = acc.uni
    balance['dash'] = acc.dash
    balance['doge'] = acc.doge
    balance['pax'] = acc.pax
    balance['leo'] = acc.leo
    balance['wbtc'] = acc.wbtc
    balance['ltc'] = acc.ltc
    balance['usdt'] = acc.usdt
    balance['bch'] = acc.bch
    balance['link'] = acc.link

    max_btc_num = (max_btc.get('fpu__max'))
    max_eth_num = (max_eth.get('fpu__max'))
    min_eth_num = (min_eth.get('fpu__max'))
    min_btc_num = (min_btc.get('fpu__min'))
    max_ltc_num = (max_ltc.get('fpu__max'))
    min_ltc_num = (min_ltc.get('fpu__min'))
    max_dash_num = (max_dash.get('fpu__max'))
    min_dash_num = (min_dash.get('fpu__min'))
    max_bch_num = (max_bch.get('fpu__max'))
    min_bch_num = (min_bch.get('fpu__min'))
    max_usdt_num = (max_usdt.get('fpu__max'))
    min_usdt_num = (max_usdt.get('fpu__min'))

    orders = Order2.objects.all()
    current_order = Order2.objects.filter(user=user)
    trans = Transactions2.objects.all()
    auth = UserAuthModel.objects.filter(user=user).first()
    cp2p = P2P2.objects.filter(user=user)
    p2p_a = P2P_Acc2.objects.filter(p2pseller=user)
    p2p_b = P2P_Acc2.objects.filter(p2pbuyer=user)

    if auth == None:
        lvl = 0
    else:
        lvl = auth.lvl
    return render(request, 'index2.html',
                  {'orders': orders, 'trans': trans, 'current_user': current_order, 'max_btc': max_btc_num,
                   'min_btc': min_btc_num, 'max_ltc': max_ltc_num, 'min_ltc': min_ltc_num, 'max_dash': max_dash_num,
                   'min_dash': min_dash_num, 'max_bch': max_bch_num, 'min_bch': min_bch_num, 'l': lvl,
                   'max_eth': max_eth_num, 'min_eth': min_eth_num, 'max_usdt': max_usdt_num, 'min_usdt': min_usdt_num,
                   'sum': get_sum, 'tsum': tsum, 'bal': balance, 'po': p2p_a, 'pa': p2p_b, 'rec': rec,'p2ps':p2ps,'obj':obj})






def trade2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        update_wage(user)

        acc = Account.objects.filter(user=user).first()
        if acc.teth < 0:
            messages.success(request, 'موجودی حساب شما کافی نمی باشد')

        bfee = {}
        sfee = {}

        today = str(datetime.datetime.now()).split()[0]
        # expire(today)
        orders = Order2.objects.all()
        current_order = Order2.objects.filter(user=user)

        buyers = Order2.objects.filter(cryp='btc').filter(trans_type='b').filter(order_type='n').order_by('fpu')
        sellers = Order2.objects.filter(cryp='btc').filter(trans_type='s').filter(order_type='n').order_by(
            'fpu').reverse()

        for i in buyers:
            if (float(i.remnant) > 0):
                if (float(i.fpu) in bfee):
                    bfee[float(i.fpu)] += float(i.remnant)
                else:
                    bfee[float(i.fpu)] = float(i.remnant)

        for i in sellers:
            if (float(i.remnant) > 0):
                if (float(i.fpu) in sfee):
                    sfee[float(i.fpu)] += float(i.remnant)
                else:
                    sfee[float(i.fpu)] = float(i.remnant)

        return render(request, 'tabbed2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth,
                       'acc': acc, 'bu': bfee, 'se': sfee, 'curr': current_order})
    else:
        bform = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if bform['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, cryp='btc', limited=l, fpu=bform['fpu'].value(), amount=bform['amount'].value(),
                      remnant=bform['amount'].value())

        if bform['date'].value() != None:
            date = bform['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'
        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        if order.trans_type == 'b' and order.order_type == 'n' \
                and float(acc.teth) < float(order.fpu) * float(order.amount) * (1 + float(acc.wage) / 100):
            messages.success(request, 'موجودی ریالی حساب شما کافی نمی باشد')

        elif order.trans_type == 's' and float(getattr(acc, order.cryp)) < float(order.amount):
            messages.success(request, 'موجودی ارزی حساب شما کافی نمی باشد')

        elif order.trans_type == 's' and order.order_type == 'n' \
                and float(acc.teth) < float(order.amount) * float(order.fpu) * (float(acc.wage) / 100):
            messages.success(request, 'موجودی ریالی حساب شما برای پرداخت کارمزد کافی نمی باشد')
        else:

            fix_order(order, acc)
            block = BlockedCoin.objects.filter(block=acc).first()
            print(block.teth)
            match(order)

        return redirect('/account/trade_btc2/')


def trade_eth2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm
        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        if acc.eth_address == None:
            set_address(acc, 'eth')
        return render(request, 'trade-eth2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, cryp='eth', limited=l, fpu=form['fpu'].value(), amount=form['amount'].value(),
                      budget=form['budget'].value(), remnant=form['amount'].value())
        if form['date'].value() != None:
            date = form['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        fix_order(order, acc)
        match(order)

        return redirect('/account/trade-eth2')


def trade_usdt2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        return render(request, 'trade-usdt2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, from_cryp='teth', to_cryp='btc', limited=l, fpu=form['fpu'].value(),
                      amount=form['amount'].value(),
                      budget=form['budget'].value())
        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'

        order.save()

        return redirect('/account/trade-usdt2')


def trade_ltc2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        if acc.ltc_address == None:
            set_address(acc, 'ltc')
        return render(request, 'trade-ltc2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'

        order = Order2(user=user, cryp='ltc', limited=l, fpu=form['fpu'].value(), amount=form['amount'].value(),
                      budget=form['budget'].value(), remnant=form['amount'].value())

        if form['date'].value() != None:
            date = form['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        fix_order(order, acc)
        match(order)

        return redirect('/account/trade-ltc2')


def trade_bch2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        return render(request, 'trade-bch2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, from_cryp='teth', to_cryp='btc', limited=l, fpu=form['fpu'].value(),
                      amount=form['amount'].value(),
                      budget=form['budget'].value())
        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'

        order.save()

        return redirect('/account/trade-bch2')


def reports2(request):
    user = User.objects.filter(id=request.user.id).first()
    curr = Order2.objects.filter(user=user)
    acc = Account.objects.filter(user=user)
    trans = Transactions2.objects.filter(buyer=user)

    return render(request, 'reports2.html', {'curr': curr, 'acc': acc, 'trans': trans})




def trade_dash2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        if acc.doge_address == None:
            set_address(acc, 'dash')
        return render(request, 'tabbed2.html', {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'

        order = Order2(user=user, cryp='doge', limited=l, fpu=form['fpu'].value(), amount=form['amount'].value(),
                      budget=form['budget'].value(), remnant=form['amount'].value())

        if form['date'].value() != None:
            date = form['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        fix_order(order, acc)
        match(order)

        return redirect('/account/trade_btc2')





def error_404(request, exception):
    return render(request, 'error-62.html')


def error_500(request, exception):
    data = {}

    return render(request, 'error-62.html', data
                  )


def redirect_view(request):
    response = redirect('/account/home2')
    return response


def trade_leo2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        if acc.btc_address == None:
            set_address(acc, 'btc')
        return render(request, 'trade_leo2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, cryp='leo', limited=l, fpu=form['fpu'].value(), amount=form['amount'].value(),
                      budget=form['budget'].value(), remnant=form['amount'].value())

        if form['date'].value() != None:
            date = form['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'
        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        fix_order(order, acc)
        match(order)

        return redirect('/account/trade_leo2')


def trade_uni2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        if acc.btc_address == None:
            set_address(acc, 'btc')
        return render(request, 'trade-uni2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, cryp='uni', limited=l, fpu=form['fpu'].value(), amount=form['amount'].value(),
                      budget=form['budget'].value(), remnant=form['amount'].value())

        if form['date'].value() != None:
            date = form['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'
        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        fix_order(order, acc)
        match(order)

        return redirect('/account/trade_uni2')


def trade_maker2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        if acc.btc_address == None:
            set_address(acc, 'btc')
        return render(request, 'trade-maker2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, cryp='maker', limited=l, fpu=form['fpu'].value(), amount=form['amount'].value(),
                      budget=form['budget'].value(), remnant=form['amount'].value())

        if form['date'].value() != None:
            date = form['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'
        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        fix_order(order, acc)
        match(order)

        return redirect('/account/trade_maker2')


def trade_pax2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        if acc.btc_address == None:
            set_address(acc, 'btc')
        return render(request, 'trade-pax2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, cryp='pax', limited=l, fpu=form['fpu'].value(), amount=form['amount'].value(),
                      budget=form['budget'].value(), remnant=form['amount'].value())

        if form['date'].value() != None:
            date = form['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'
        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        fix_order(order, acc)
        match(order)

        return redirect('/account/trade_pax2')


def trade_link2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        if acc.btc_address == None:
            set_address(acc, 'btc')
        return render(request, 'trade-link2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, cryp='link', limited=l, fpu=form['fpu'].value(), amount=form['amount'].value(),
                      budget=form['budget'].value(), remnant=form['amount'].value())

        if form['date'].value() != None:
            date = form['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'
        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        fix_order(order, acc)
        match(order)

        return redirect('/account/trade_link2')


def p2p_sale2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_sale2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        order.status = '1'
        order.save()
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.status = '0'
        order.save()
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_detail_btc2(request, slug):
    user = User.objects.filter(id=request.user.id).first()
    acc = Account.objects.filter(user=user).first()
    coin = get_object_or_404(P2P_Acc2, slug=slug)
    w = float(acc.wage) / 100

    if request.method == 'GET':
        if user.id == coin.porder.user.id:
            r = 's'
        else:
            r = 'b'
        mw = str(getattr(acc, str(coin.porder.cryp) + '_address'))
        ow = UserWallet.objects.filter(user=user)

        return render(request, 'p2p_detail_btc2.html', {'coin': coin, 'role': r, 'slug': slug,
                                                       'acc': acc, 'mi': float(coin.porder.minimum),
                                                       'ma': min(coin.porder.amount,
                                                                 float(acc.teth) / (float(coin.porder.fpu) * (1 + (w/100)))),
                                                       'mw': mw, 'ow': ow})

    elif request.method == 'POST':
        blocked = BlockedCoin.objects.filter(block=acc).first()
        if 'amount' in request.POST:
            coin.amount = float(request.POST['amount'])
            coin.wallet = str(request.POST['wallet'])
            coin.step = 2
            coin.save()
            setattr(blocked, 'teth',
                    float(getattr(blocked, 'teth')) + (float(coin.amount) * float(coin.porder.fpu)) * (1 + w))

            setattr(acc, 'teth',
                    float(getattr(acc, 'teth') - (float(coin.amount) * float(coin.porder.fpu)) * (1 + w)))

            blocked.save()
            acc.save()
            return redirect('/account/' + str(slug) + '/2')


        elif 'tsid' in request.POST:
            coin.tsid = int(request.POST['tsid'])
            coin.step = 3
            coin.save()
            return redirect('/account/' + str(slug) + '/2')


        elif 'bdel' in request.POST:

            setattr(blocked, 'teth',
                    float(getattr(blocked, 'teth')) - (float(coin.amount) * float(coin.porder.fpu)) * (1 + w))

            setattr(acc, 'teth',
                    float(getattr(acc, 'teth')) + (float(coin.amount) * float(coin.porder.fpu)) * (1 + w))

            coin.delete()
            acc.save()
            blocked.save()

            return redirect('/account/p2p_sale2/')


        elif 'sdel' in request.POST:
            us = User.objects.filter(id=coin.p2pbuyer.id).first()
            ac = Account.objects.filter(user=us).first()
            bk = BlockedCoin.objects.filter(block=ac).first()
            wb = float(ac.wage) / 100

            setattr(bk, 'teth',
                    float(getattr(bk, 'teth')) - (float(coin.amount) * float(coin.porder.fpu)) * (1 + wb))

            setattr(ac, 'teth',
                    float(getattr(ac, 'teth')) + (float(coin.amount) * float(coin.porder.fpu)) * (1 + wb))

            coin.delete()
            ac.save()
            bk.save()
            return redirect('/account/home2/')


        elif 'ssell' in request.POST:
            us = User.objects.filter(id=coin.p2pbuyer.id).first()
            ac = Account.objects.filter(user=us).first()
            bk = BlockedCoin.objects.filter(block=ac).first()
            wb = float(ac.wage) / 100

            ot = int(str(coin.time).split(':')[1])
            ct = int(datetime.datetime.now().minute)
            print(ct,ot)
            # if ct < ot:
            #     ct += 60
            if (ct - ot <= 100000):
                setattr(acc, 'teth',
                        float(getattr(acc, 'teth')) + (float(coin.amount) * float(coin.porder.fpu)) * (1 + w))
                setattr(bk, 'teth',
                        float(getattr(bk, 'teth')) - (float(coin.amount) * float(coin.porder.fpu)) * (1 + wb))

                acc.save()
                bk.save()

                trans = Transactions2(seller=user, buyer=us, amount=coin.amount, fpu=coin.porder.fpu,
                                     trans_type='p2p', cryp=coin.porder.cryp)
                print(trans)

                trans.save()

                order = P2P2.objects.filter(id=coin.porder.id).first()
                order.status = '2'
                order.save()
                # order.delete()


            else:

                setattr(bk, 'teth',
                        float(getattr(bk, 'teth')) - (float(coin.amount) * float(coin.porder.fpu)) * (1 + wb))

                setattr(ac, 'teth',
                        float(getattr(ac, 'teth')) + (float(coin.amount) * float(coin.porder.fpu)) * (1 + wb))

                ac.save()
                bk.save()

            coin.delete()
            return redirect('/account/home2/')


def trade_wbtc2(request):
    if request.method == 'GET':
        bform = NormalBuyForm
        fbform = FastBuyForm
        sform = NormalSellForm
        fsform = FastSellForm

        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()
        if acc.btc_address == None:
            set_address(acc, 'btc')
        return render(request, 'trade-wbtc2.html',
                      {'f1': bform, 'f2': fbform, 'f3': sform, 'f4': fsform, 'dep': acc.teth})
    else:
        form = NormalBuyForm(request.POST)
        user = User.objects.filter(id=request.user.id).first()

        if form['type'].value() == '1':
            l = 'y'
        else:
            l = 'n'
        order = Order2(user=user, cryp='wbtc', limited=l, fpu=form['fpu'].value(), amount=form['amount'].value(),
                      budget=form['budget'].value(), remnant=form['amount'].value())

        if form['date'].value() != None:
            date = form['date'].value().split()[0].split("/")
            date = date[2] + '-' + date[0] + '-' + date[1]
            order.due_date = date

        if 'buy' in request.POST:
            order.order_type = 'n'
            order.trans_type = 'b'
        elif 'sell' in request.POST:
            order.order_type = 'n'
            order.trans_type = 's'
        elif 'fsell' in request.POST:
            order.order_type = 'f'
            order.trans_type = 's'
        elif 'fbuy' in request.POST:
            order.order_type = 'f'
            order.trans_type = 'b'
        user = User.objects.filter(id=request.user.id).first()
        acc = Account.objects.filter(user=user).first()

        fix_order(order, acc)
        match(order)

        return redirect('/account/trade-wbtc2')


def p2p_sale_recent2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user).filter(status='0')
        porders = porders.order_by('time')
        return render(request, 'p2p_sale2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=request.POST['buy']).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_lowest2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        porders = porders.order_by('amount')
        return render(request, 'p2p_sale2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=request.POST['buy']).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_highest2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        porders = porders.order_by('-amount')
        return render(request, 'p2p_sale2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=request.POST['buy']).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.save()
        return redirect('/account/' + str(p.slug)+ '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_max2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        porders = porders.order_by('fpu')
        return render(request, 'p2p_sale2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=request.POST['buy']).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.save()
        return redirect('/account/' + str(p.slug)+ '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_min2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        porders = porders.order_by('-fpu')
        return render(request, 'p2p_sale2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=request.POST['buy']).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.save()
        return redirect('/account/' + str(p.slug)+ '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_BCH2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_bch2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug)+ '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_BTG2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_btg2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_ZIL2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_zil2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_LSK2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_lsk2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_OMG2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_omg2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_KMD2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_kmd2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_DOGE2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_doge2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_ONT2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_ont2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_STEEM2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_steem2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_XVG2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_xvg2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_AE2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_ae2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_ICX2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_icx2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_QTUM2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_qtum2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_ZEC2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_zec2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_BCN2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_bcn2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_BNB2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_bnb2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_ETC2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_etc2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_VET2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_vet2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_XEM2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_xem2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_XMR2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_xmr2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_NEO2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_neo2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_TRX2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_trx2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_MIOTA2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_miota2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_XLM2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_xlm2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_ADA2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_ada2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def p2p_sale_EOS2(request):
    user = User.objects.filter(id=request.user.id).first()
    '''p2p = P2P2(user=user , minimum=7 , fpu=200 , cryp='btc')
    p2p.save()'''
    if request.method == 'GET':
        form = P2PCreateForm

        porders = P2P2.objects.exclude(user=user)
        return render(request, 'p2p_eos2.html', {'po': porders, 'form': form})
    if request.method == 'POST' and 'deposit' not in request.POST:
        order = P2P2.objects.filter(id=int(request.POST['buy'])).first()
        seller = order.user
        buyer = user
        p = P2P_Acc2(p2pbuyer=buyer, p2pseller=seller, porder=order
                    , rand=str(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))))
        p.slug = slugify(str(seller.id) + '_' + str(buyer.id) + '_' + str(p.rand))
        p.save()
        return redirect('/account/' + str(p.slug) + '/2')
    if request.method == 'POST' and 'deposit' in request.POST:
        form = P2PCreateForm(request.POST)

        order = P2P2.objects.create(fpu=form['fpu'].value(), amount=form['amount'].value(), user=user,
                                   cryp=form['cryp'].value(), minimum=form['minimum'].value())
        order.save()
        print(order)
        return redirect('account:home2')
    else:

        return HttpResponse('ERROR')


def first_page2(request):
    return render(request, 'home2.html')


def log_out2(request):
    logout(request)
    return redirect('account:home2')


def delete_item2(request, id):
    order = Order2.objects.filter(id=id).filter(remnant__gt=0).first()
    cryp = order.cryp
    otype = order.order_type
    ttype = order.trans_type
    rem = float(order.remnant)
    user = order.user
    acc = Account.objects.filter(user=user).first()
    buser = BlockedCoin.objects.filter(block=acc).first()
    fpu = float(order.fpu)
    if rem > 0:
        if (ttype == 'b'):
            buser.teth = float(buser.teth) - fpu * rem * (1 + float(acc.wage) / 100)
            acc.teth = float(acc.teth) + fpu * rem * (1 + float(acc.wage) / 100)
        else:
            setattr(buser, cryp, float(getattr(buser, cryp)) - float(rem))
            setattr(acc, cryp, float(getattr(acc, cryp)) + float(rem))
            buser.teth = float(buser.teth) - fpu * rem * (1 + float(acc.wage) / 100)
            acc.teth = float(acc.teth) + fpu * rem * (1 + float(acc.wage) / 100)

        buser.save()
        acc.save()
        order.delete()

    return redirect('/account/trade_btc2/')






