from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from extensions.utils import jalali_converter
import qrcode
from io import BytesIO
from django.core.files import File
from PIL import Image, ImageDraw
from django.utils.text import slugify
from django.urls import reverse
from extensions.utils import generate_ref_code


# Create your models here.
class User(AbstractUser):
    email = models.EmailField(unique=True, verbose_name='ایمیل')
    special_user = models.DateTimeField(default=timezone.now, verbose_name="کاربر ویژه تا")

    def __str__(self):
        return self.email

    def is_special_user(self):
        if self.special_user > timezone.now():
            return True
        else:
            return False

    is_special_user.boolean = True
    is_special_user.short_description = "وضعیت کاربر "


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    wage = models.FloatField(default=0.4)

    rial = models.FloatField(default=0)
    teth = models.FloatField(default=0)
    btc = models.FloatField(default=0)
    eth = models.FloatField(default=0)
    bch = models.FloatField(default=0)
    ltc = models.FloatField(default=0)
    dash = models.FloatField(default=0)
    usdt = models.FloatField(default=0)
    etc = models.FloatField(default=0)
    bnb = models.FloatField(default=0)
    xrp = models.FloatField(default=0)
    trx = models.FloatField(default=0)
    dgb = models.FloatField(default=0)
    bsr = models.FloatField(default=0)
    doge = models.FloatField(default=0)
    link = models.FloatField(default=0)
    platon = models.FloatField(default=0)
    xcon = models.FloatField(default=0)
    wbtc = models.FloatField(default=0)
    free = models.FloatField(default=0)
    leo = models.FloatField(default=0)
    maker = models.FloatField(default=0)
    pax = models.FloatField(default=0)
    paxg = models.FloatField(default=0)
    tusd = models.FloatField(default=0)
    uni = models.FloatField(default=0)
    ada = models.FloatField(default=0)
    eos = models.FloatField(default=0)
    xlm = models.FloatField(default=0)
    xtz = models.FloatField(default=0)
    atom = models.FloatField(default=0)
    xmr = models.FloatField(default=0)
    blocked = models.FloatField(default=0)
    btc_address = models.CharField(max_length=100, null=True)
    dash_address = models.CharField(max_length=100, null=True)
    eth_address = models.CharField(max_length=100, null=True)
    ltc_address = models.CharField(max_length=100, null=True)
    xlm_address = models.CharField(max_length=100, null=True)
    leo_address = models.CharField(max_length=100, null=True)
    pax_address = models.CharField(max_length=100, null=True)
    paxg_address = models.CharField(max_length=100, null=True)
    tusd_address = models.CharField(max_length=100, null=True)
    platon_address = models.CharField(max_length=100, null=True)
    uni_address = models.CharField(max_length=100, null=True)
    maker_address = models.CharField(max_length=100, null=True)
    free_address = models.CharField(max_length=100, null=True)
    wbtc_address = models.CharField(max_length=100, null=True)
    xcon_address = models.CharField(max_length=100, null=True)
    link_address = models.CharField(max_length=100, null=True)
    bch_address = models.CharField(max_length=100, null=True)
    btg_address = models.CharField(max_length=100, null=True)
    zec_address = models.CharField(max_length=100, null=True)
    doge_address = models.CharField(max_length=100, null=True)
    btc_qr = models.ImageField(upload_to="images")
    eth_qr = models.ImageField(upload_to="images")
    dash_qr = models.ImageField(upload_to="images")
    ltc_qr = models.ImageField(upload_to="images")
    zec_qr = models.ImageField(upload_to="images")
    xlm_qr = models.ImageField(upload_to="images")
    bch_qr = models.ImageField(upload_to="images")
    btg_qr = models.ImageField(upload_to="images")
    xcon_qr = models.ImageField(upload_to="images")
    pax_qr = models.ImageField(upload_to="images")
    paxg_qr = models.ImageField(upload_to="images")
    leo_qr = models.ImageField(upload_to="images")
    tusd_qr = models.ImageField(upload_to="images")
    uni_qr = models.ImageField(upload_to="images")
    maker_qr = models.ImageField(upload_to="images")
    wbtc_qr = models.ImageField(upload_to="images")
    free_qr = models.ImageField(upload_to="images")
    link_qr = models.ImageField(upload_to="images")
    platon_qr = models.ImageField(upload_to="images")

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = 'مدیریت اکانت ها'
        verbose_name_plural = 'مدیریت اکانت ها'

    def save(self, *args, **kwargs):
        leo_image = qrcode.make(self.leo_address)
        link_image = qrcode.make(self.link_address)
        pax_image = qrcode.make(self.pax_address)
        paxg_image = qrcode.make(self.paxg_address)
        platon_image = qrcode.make(self.platon_address)
        uni_image = qrcode.make(self.uni_address)
        maker_image = qrcode.make(self.maker_address)
        free_image = qrcode.make(self.free_address)
        wbtc_image = qrcode.make(self.wbtc_address)
        tusd_image = qrcode.make(self.tusd_address)
        xcon_image = qrcode.make(self.xcon_address)

        btc_image = qrcode.make(self.btc_address)
        dash_image = qrcode.make(self.dash_address)
        zec_image = qrcode.make(self.zec_address)
        # 		xlm_image=qrcode.make(self.xlm_address)
        ltc_image = qrcode.make(self.ltc_address)
        bch_image = qrcode.make(self.bch_address)
        btg_image = qrcode.make(self.btg_address)
        eth_image = qrcode.make(self.eth_address)
        btc_canvas = Image.new('RGB', (400, 400), 'white')
        dash_canvas = Image.new('RGB', (400, 400), 'white')
        ltc_canvas = Image.new('RGB', (400, 400), 'white')
        zec_canvas = Image.new('RGB', (400, 400), 'white')
        # 		xlm_canvas= Image.new('RGB',(400,400),'white')
        btg_canvas = Image.new('RGB', (400, 400), 'white')
        bch_canvas = Image.new('RGB', (400, 400), 'white')
        eth_canvas = Image.new('RGB', (400, 400), 'white')
        pax_canvas = Image.new('RGB', (400, 400), 'white')
        paxg_canvas = Image.new('RGB', (400, 400), 'white')
        uni_canvas = Image.new('RGB', (400, 400), 'white')
        xcon_canvas = Image.new('RGB', (400, 400), 'white')
        tusd_canvas = Image.new('RGB', (400, 400), 'white')
        wbtc_canvas = Image.new('RGB', (400, 400), 'white')
        leo_canvas = Image.new('RGB', (400, 400), 'white')
        free_canvas = Image.new('RGB', (400, 400), 'white')
        platon_canvas = Image.new('RGB', (400, 400), 'white')
        maker_canvas = Image.new('RGB', (400, 400), 'white')
        link_canvas = Image.new('RGB', (400, 400), 'white')
        btc_draw = ImageDraw.Draw(btc_canvas)
        link_draw = ImageDraw.Draw(link_canvas)
        pax_draw = ImageDraw.Draw(pax_canvas)
        paxg_draw = ImageDraw.Draw(paxg_canvas)
        wbtc_draw = ImageDraw.Draw(wbtc_canvas)
        platon_draw = ImageDraw.Draw(platon_canvas)
        uni_draw = ImageDraw.Draw(uni_canvas)
        free_draw = ImageDraw.Draw(free_canvas)
        tusd_draw = ImageDraw.Draw(tusd_canvas)
        xcon_draw = ImageDraw.Draw(xcon_canvas)
        leo_draw = ImageDraw.Draw(leo_canvas)
        maker_draw = ImageDraw.Draw(maker_canvas)
        dash_draw = ImageDraw.Draw(dash_canvas)
        ltc_draw = ImageDraw.Draw(ltc_canvas)
        zec_draw = ImageDraw.Draw(zec_canvas)
        # 		xlm_draw = ImageDraw.Draw(xlm_canvas)
        btg_draw = ImageDraw.Draw(btg_canvas)
        bch_draw = ImageDraw.Draw(bch_canvas)
        eth_draw = ImageDraw.Draw(eth_canvas)
        btc_canvas.paste(btc_image)
        dash_canvas.paste(dash_image)
        ltc_canvas.paste(ltc_image)
        # 		xlm_canvas.paste(xlm_image)
        bch_canvas.paste(bch_image)
        btg_canvas.paste(btg_image)
        zec_canvas.paste(zec_image)
        eth_canvas.paste(eth_image)
        pax_canvas.paste(pax_image)
        paxg_canvas.paste(paxg_image)
        uni_canvas.paste(uni_image)
        link_canvas.paste(link_image)
        wbtc_canvas.paste(wbtc_image)
        platon_canvas.paste(platon_image)
        tusd_canvas.paste(tusd_image)
        free_canvas.paste(free_image)
        maker_canvas.paste(maker_image)
        xcon_canvas.paste(xcon_image)
        leo_canvas.paste(leo_image)
        dash_fname = f'dash_qr-{self.dash_address}' + '.png'
        leo_fname = f'leo_qr-{self.leo_address}' + '.png'
        uni_fname = f'uni_qr-{self.uni_address}' + '.png'
        pax_fname = f'pax_qr-{self.pax_address}' + '.png'
        paxg_fname = f'paxg_qr-{self.paxg_address}' + '.png'
        free_fname = f'free_qr-{self.free_address}' + '.png'
        maker_fname = f'maker_qr-{self.maker_address}' + '.png'
        platon_fname = f'platon_qr-{self.platon_address}' + '.png'
        tusd_fname = f'tusd_qr-{self.tusd_address}' + '.png'
        wbtc_fname = f'wbtc_qr-{self.wbtc_address}' + '.png'
        xcon_fname = f'xcon_qr-{self.xcon_address}' + '.png'
        link_fname = f'link_qr-{self.link_address}' + '.png'
        btc_fname = f'btc_qr-{self.btc_address}' + '.png'
        ltc_fname = f'ltc_qr-{self.ltc_address}' + '.png'
        zec_fname = f'zec_qr-{self.zec_address}' + '.png'
        # 		xlm_fname = f'xlm_qr-{self.xlm_address}'+'.png'
        btg_fname = f'btg_qr-{self.btg_address}' + '.png'
        bch_fname = f'bch_qr-{self.bch_address}' + '.png'
        eth_fname = f'eth_qr-{self.eth_address}' + '.png'
        btc_Buffer = BytesIO()
        bch_Buffer = BytesIO()
        zec_Buffer = BytesIO()
        # 		xlm_Buffer = BytesIO()
        ltc_Buffer = BytesIO()
        btg_Buffer = BytesIO()
        dash_Buffer = BytesIO()
        eth_Buffer = BytesIO()
        wbtc_Buffer = BytesIO()
        link_Buffer = BytesIO()
        free_Buffer = BytesIO()
        tusd_Buffer = BytesIO()
        maker_Buffer = BytesIO()
        pax_Buffer = BytesIO()
        paxg_Buffer = BytesIO()
        uni_Buffer = BytesIO()
        platon_Buffer = BytesIO()
        xcon_Buffer = BytesIO()
        leo_Buffer = BytesIO()
        btc_canvas.save(btc_Buffer, 'PNG')
        link_canvas.save(link_Buffer, 'PNG')
        dash_canvas.save(dash_Buffer, 'PNG')
        ltc_canvas.save(ltc_Buffer, 'PNG')
        zec_canvas.save(zec_Buffer, 'PNG')
        # 		xlm_canvas.save(xlm_Buffer,'PNG')
        bch_canvas.save(bch_Buffer, 'PNG')
        eth_canvas.save(eth_Buffer, 'PNG')
        btg_canvas.save(btg_Buffer, 'PNG')
        leo_canvas.save(leo_Buffer, 'PNG')
        pax_canvas.save(pax_Buffer, 'PNG')
        paxg_canvas.save(paxg_Buffer, 'PNG')
        tusd_canvas.save(tusd_Buffer, 'PNG')
        xcon_canvas.save(xcon_Buffer, 'PNG')
        wbtc_canvas.save(wbtc_Buffer, 'PNG')
        uni_canvas.save(uni_Buffer, 'PNG')
        platon_canvas.save(platon_Buffer, 'PNG')
        free_canvas.save(free_Buffer, 'PNG')
        maker_canvas.save(maker_Buffer, 'PNG')
        self.btc_qr.save(btc_fname, File(btc_Buffer), save=False)
        self.dash_qr.save(dash_fname, File(dash_Buffer), save=False)
        self.ltc_qr.save(ltc_fname, File(ltc_Buffer), save=False)
        self.zec_qr.save(zec_fname, File(zec_Buffer), save=False)
        # 		self.xlm_qr.save(xlm_fname,File(xlm_Buffer),save=False)
        self.btg_qr.save(btg_fname, File(btg_Buffer), save=False)
        self.bch_qr.save(bch_fname, File(bch_Buffer), save=False)
        self.eth_qr.save(eth_fname, File(eth_Buffer), save=False)
        self.leo_qr.save(leo_fname, File(leo_Buffer), save=False)
        self.xcon_qr.save(xcon_fname, File(xcon_Buffer), save=False)
        self.link_qr.save(link_fname, File(link_Buffer), save=False)
        self.uni_qr.save(uni_fname, File(uni_Buffer), save=False)
        self.tusd_qr.save(tusd_fname, File(tusd_Buffer), save=False)
        self.wbtc_qr.save(wbtc_fname, File(wbtc_Buffer), save=False)
        self.platon_qr.save(platon_fname, File(platon_Buffer), save=False)
        self.paxg_qr.save(paxg_fname, File(paxg_Buffer), save=False)
        self.pax_qr.save(pax_fname, File(pax_Buffer), save=False)
        self.maker_qr.save(maker_fname, File(maker_Buffer), save=False)
        self.free_qr.save(free_fname, File(free_Buffer), save=False)
        btc_canvas.close()
        dash_canvas.close()
        ltc_canvas.close()
        bch_canvas.close()
        btg_canvas.close()
        # 		xlm_canvas.close()
        zec_canvas.close()
        eth_canvas.close()
        platon_canvas.close()
        wbtc_canvas.close()
        uni_canvas.close()
        link_canvas.close()
        paxg_canvas.close()
        pax_canvas.close()
        tusd_canvas.close()
        xcon_canvas.close()
        maker_canvas.close()
        free_canvas.close()
        leo_canvas.close()
        super().save(*args, **kwargs)


class Tickets(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(null=False)
    status = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ticket_date = models.DateTimeField(auto_now_add=True)

    def jpublish(self):
        return jalali_converter(self.ticket_date)

    jpublish.short_description = "زمان انتشار"

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'تیکت'
        verbose_name_plural = 'تیکت ها'


class Transactions(models.Model):
    buyer = models.ForeignKey(User, on_delete=models.PROTECT, related_name='buyer')
    seller = models.ForeignKey(User, on_delete=models.PROTECT, related_name='seller')
    date = models.DateTimeField(auto_now=True, auto_now_add=False)
    cryp = models.CharField(null=True, max_length=4)
    trans_type = models.CharField(null=True, max_length=10)
    from_address = models.CharField(max_length=100, null=True)
    to_address = models.CharField(max_length=100, null=True)
    amount = models.FloatField(null=False, default=0)
    fpu = models.FloatField(null=True, default=0)

    def jpublish(self):
        return jalali_converter(self.date)

    jpublish.short_description = "زمان انتشار"

    # def __str__(self):
    #     return (str(self.buyer), str(self.seller))

    class Meta:
        verbose_name = 'تراکنش'
        verbose_name_plural = 'تراکنش ها'


# STAT_CHOICES = (
# 	("p", "pending"),
# 	("d", "done"),
# 	("e", "expired"),
#
# )
# buyer = models.ForeignKey(User , on_delete=models.PROTECT , related_name='buyer')
# seller = models.ForeignKey(User , on_delete=models.PROTECT, related_name='seller')
# date = models.DateTimeField(default=timezone.now)
# description = models.TextField()
# status = models.CharField(max_length=20,
#               choices=STAT_CHOICES,
#               default="p")
# from_address = models.CharField(max_length=100, null=True)
# to_address = models.CharField(max_length=100, null=True)
#
# def  __str__(self):
# 	return self.buyer
#
# class Meta:
# 	verbose_name = 'تراکنش'
# 	verbose_name_plural = 'تراکنش ها'


class Order(models.Model):
    TRANSACTION_TYPES = (
        ("s", "sell"),
        ("b", "buy"),
        ("e", "exchange"),

    )
    ORDER_TYPES = (
        ("n", "normal"),
        ("f", "fast"),
    )
    LIMIT_TYPES = (
        ("y", "yes"),
        ("n", "no"),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    trans_type = models.CharField(max_length=20,
                                  choices=TRANSACTION_TYPES, null=True)
    cryp = models.CharField(max_length=4, null=True)
    time = models.DateTimeField(auto_now=True, auto_now_add=False)
    due_date = models.DateField(null=True)
    order_type = models.CharField(max_length=20,
                                  choices=ORDER_TYPES, null=True)
    limited = models.CharField(max_length=20,
                               choices=LIMIT_TYPES, default="n")
    fpu = models.FloatField(default=0, null=True)
    budget = models.FloatField(default=0, null=True)
    amount = models.FloatField(default=0, null=True)
    remnant = models.FloatField(null=True)
    description = models.CharField(max_length=100, null=True)

    def jpublish(self):
        return jalali_converter(self.time)

    jpublish.short_description = "زمان انتشار"

    def __str__(self):
        return self.trans_type

    def total(self):
        return ((self.amount) * (self.fpu))

    class Meta:
        verbose_name = 'سفارش'
        verbose_name_plural = 'سفارشات'


class UserAuthModel(models.Model):
    STATUS = (
        ("v", "verified"),
        ("d", "dismissed"),

    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, null=False)
    last_name = models.CharField(max_length=100, null=False)
    melli = models.CharField(max_length=15, null=False)
    sheba = models.CharField(max_length=25, null=False)
    card = models.CharField(max_length=20, null=False)
    status = models.CharField(choices=STATUS, null=True, max_length=10)
    pic = models.ImageField(upload_to="images")
    lvl = models.IntegerField(null=True)
    mobile = models.CharField(max_length=100, null=True, default="")
    phone = models.CharField(max_length=100, null=True, default="")
    add = models.CharField(max_length=100, null=False, default="")

    def __str__(self):
        return ((self.first_name) + ' _ ' + (self.last_name))

    class Meta:
        verbose_name = 'احراز هویت'
        verbose_name_plural = 'احراز های هویت'


class Bank_Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sheba = models.CharField(max_length=25, null=False)
    card = models.CharField(max_length=20, null=False)


class BlockedCoin(models.Model):
    block = models.ForeignKey(Account, on_delete=models.CASCADE)
    rial = models.FloatField(default=0, null=False)
    teth = models.FloatField(default=0)
    btc = models.FloatField(default=0, null=False)
    eth = models.FloatField(default=0, null=False)
    bch = models.FloatField(default=0, null=False)
    ltc = models.FloatField(default=0, null=False)
    dash = models.FloatField(default=0, null=False)
    usdt = models.FloatField(default=0, null=False)
    etc = models.FloatField(default=0, null=False)
    bnb = models.FloatField(default=0, null=False)
    xrp = models.FloatField(default=0, null=False)
    trx = models.FloatField(default=0, null=False)
    dgb = models.FloatField(default=0, null=False)
    bsr = models.FloatField(default=0, null=False)
    doge = models.FloatField(default=0, null=False)
    link = models.FloatField(default=0, null=False)
    ada = models.FloatField(default=0, null=False)
    eos = models.FloatField(default=0, null=False)
    xlm = models.FloatField(default=0, null=False)
    xtz = models.FloatField(default=0, null=False)
    atom = models.FloatField(default=0, null=False)
    xmr = models.FloatField(default=0, null=False)
    pax = models.FloatField(default=0, null=False)
    paxg = models.FloatField(default=0, null=False)
    uni = models.FloatField(default=0, null=False)
    platon = models.FloatField(default=0, null=False)
    tusd = models.FloatField(default=0, null=False)
    xcon = models.FloatField(default=0, null=False)
    free = models.FloatField(default=0, null=False)
    maker = models.FloatField(default=0, null=False)
    wbtc = models.FloatField(default=0, null=False)
    link = models.FloatField(default=0, null=False)
    leo = models.FloatField(default=0, null=False)


class P2P(models.Model):
    STATUS = (
        ("0", "در انتظار خریدار"),
        ("1", "در انتظار تایید"),
        ("2", "انجام شده"),

    )
    CRYPTOS = [
        ("btc", "bitcoin"),
        ("ltc", "litecoin"),
        ("pax", "pax"),
        ("uni", "uniswap"),
        ("maker", "maker"),
        ("dash", "dashcoin"),
        ("eth", "ethereum"),
        ("xrp", "ripple"),
        ("bch", "bitcoincash"),
        ("eos", "eos"),
        ("ada", "kardano"),
        ("xlm", "stellar"),
        ("miota", "itota"),
        ("trx", "teron"),
        ("neo", "neo"),
        ("xmr", "monroe"),
        ("xem", "nem"),
        ("usdt", "tether"),
        ("vet", "vchain"),
        ("etc", "ethclassic"),
        ("bnb", "binance"),
        ("zec", "zicash"),
        ("qtum", "qtum"),
        ("icx", "icon"),
        ("omg", "omisgo"),
        ("lsk", "lisk"),
        ("zil", "zilika"),
        ("btg", "bitcoingold"),
        ("ae", "eternity"),
        ("doge", "doge"),
        ("kmd", "komodo"),
        ("wm", "webmoney"),
    ]
    time = models.DateTimeField(auto_now=True, auto_now_add=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cryp = models.CharField(max_length=6, null=True, choices=CRYPTOS)
    status = models.CharField(max_length=30, null=True, choices=STATUS)
    title = models.CharField(max_length=100, null=True)
    minimum = models.FloatField(max_length=10, null=True)
    fpu = models.FloatField(default=0, null=True)
    amount = models.FloatField(default=0, null=True)

    def jpublish(self):
        return jalali_converter(self.time)

    jpublish.short_description = "زمان انتشار"

    def __str__(self):
        return ((self.cryp))

    class Meta:
        verbose_name = 'معاملات بی واسطه'
        verbose_name_plural = 'معامله های بی واسطه'


class P2P_Acc(models.Model):
    time = models.DateTimeField(auto_now=True, auto_now_add=False)
    porder = models.ForeignKey(P2P, on_delete=models.CASCADE, null=True)
    p2pbuyer = models.ForeignKey(User, on_delete=models.PROTECT, related_name='pbuyer')
    p2pseller = models.ForeignKey(User, on_delete=models.PROTECT, related_name='pseller')
    tsid = models.IntegerField(null=True, blank=True)
    step = models.IntegerField(default=1)
    slug = models.SlugField(unique=True, max_length=200, null=True, blank=True)
    amount = models.FloatField(max_length=10, null=True)
    rand = models.CharField(max_length=100, null=True)
    wallet = models.CharField(max_length=100, null=True)

    class Meta:
        verbose_name = 'مدیریت کاربران معامله بی واسطه'
        verbose_name_plural = 'مدیریت کاربر های معاملات بی واسظه'

    def save(self, *args, **kwargs):
        (value) = str(self.p2pseller.id) + '_' + str(self.p2pbuyer.id) + '_' + str(self.rand)
        self.slug = slugify(value)
        super(P2P_Acc, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('account:detail_btc', args=[self.slug])
    def jpublish(self):
        return jalali_converter(self.time)

    jpublish.short_description = "زمان انتشار"


class UserWallet(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    address = models.CharField(max_length=100, null=True)
    CRYPTOS = [
        ("btc", "bitcoin"),
        ("ltc", "litecoin"),
        ("pax", "pax"),
        ("uni", "uniswap"),
        ("maker", "maker"),
        ("dash", "dashcoin"),
        ("eth", "ethereum"),
        ("xrp", "ripple"),
        ("bch", "bitcoincash"),
        ("eos", "eos"),
        ("ada", "kardano"),
        ("xlm", "stellar"),
        ("miota", "itota"),
        ("trx", "teron"),
        ("neo", "neo"),
        ("xmr", "monroe"),
        ("xem", "nem"),
        ("usdt", "tether"),
        ("vet", "vchain"),
        ("etc", "ethclassic"),
        ("bnb", "binance"),
        ("zec", "zicash"),
        ("qtum", "qtum"),
        ("icx", "icon"),
        ("omg", "omisgo"),
        ("lsk", "lisk"),
        ("zil", "zilika"),
        ("btg", "bitcoingold"),
        ("ae", "eternity"),
        ("doge", "doge"),
        ("kmd", "komodo"),
        ("wm", "webmoney"),
    ]
    cryp = models.CharField(max_length=6, null=True, choices=CRYPTOS)


    class Meta:
        verbose_name = 'کیف پول کاربر'
        verbose_name_plural = 'کیف پول های کاربر'


class Profile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    code = models.CharField(max_length=12, blank=True)
    recommended_by = models.OneToOneField(User, on_delete=models.CASCADE, related_name='ref_by', blank=True, null=True)

    def get_recommended_profiles(self):
        pass

    def save(self, *args, **kwargs):
        if self.code == "":
            code = generate_ref_code()
            self.code = code
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'مدیریت زیرمجموعه '
        verbose_name_plural = 'مدیریت زیرمجموعه ها'
class Notification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now=True, auto_now_add=False)
    description = models.CharField(max_length=100, null=True)
    title = models.CharField(max_length=100, null=True)
    trans = models.ForeignKey(Transactions, on_delete=models.CASCADE,blank=True,null=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE,blank=True,null=True)
    ticket = models.ForeignKey(Tickets,on_delete=models.CASCADE,blank=True,null=True)
    p2p = models.ForeignKey(P2P, on_delete=models.CASCADE,blank=True,null=True)
    auth = models.ForeignKey(UserAuthModel, on_delete=models.CASCADE,blank=True,null=True)
    STATUS = (
        ("0", "مشاهده نشده"),
        ("1", "مشاهده شده"),

    )
    status = models.CharField(choices=STATUS,max_length=10)
    def jpublish(self):
        return jalali_converter(self.time)


















class Transactions2(models.Model):
    buyer = models.ForeignKey(User, on_delete=models.PROTECT, related_name='tbuyer')
    seller = models.ForeignKey(User, on_delete=models.PROTECT, related_name='tseller')
    date = models.DateTimeField(auto_now=True, auto_now_add=False)
    cryp = models.CharField(null=True, max_length=4)
    trans_type = models.CharField(null=True, max_length=10)
    from_address = models.CharField(max_length=100, null=True)
    to_address = models.CharField(max_length=100, null=True)
    amount = models.FloatField(null=False, default=0)
    fpu = models.FloatField(null=True, default=0)

    def jpublish(self):
        return jalali_converter(self.date)

    jpublish.short_description = "زمان انتشار"

    # def __str__(self):
    #     return (str(self.buyer), str(self.seller))

    class Meta:
        verbose_name = 'تراکنش'
        verbose_name_plural = 'تراکنش ها'



class Order2(models.Model):
    TRANSACTION_TYPES = (
        ("s", "sell"),
        ("b", "buy"),
        ("e", "exchange"),

    )
    ORDER_TYPES = (
        ("n", "normal"),
        ("f", "fast"),
    )
    LIMIT_TYPES = (
        ("y", "yes"),
        ("n", "no"),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    trans_type = models.CharField(max_length=20,
                                  choices=TRANSACTION_TYPES, null=True)
    cryp = models.CharField(max_length=4, null=True)
    time = models.DateTimeField(auto_now=True, auto_now_add=False)
    due_date = models.DateField(null=True)
    order_type = models.CharField(max_length=20,
                                  choices=ORDER_TYPES, null=True)
    limited = models.CharField(max_length=20,
                               choices=LIMIT_TYPES, default="n")
    fpu = models.FloatField(default=0, null=True)
    budget = models.FloatField(default=0, null=True)
    amount = models.FloatField(default=0, null=True)
    remnant = models.FloatField(null=True)
    description = models.CharField(max_length=100, null=True)

    def jpublish(self):
        return jalali_converter(self.time)

    jpublish.short_description = "زمان انتشار"

    def __str__(self):
        return self.trans_type

    def total(self):
        return ((self.amount) * (self.fpu))

    class Meta:
        verbose_name = 'سفارش'
        verbose_name_plural = 'سفارشات'



class P2P2(models.Model):
    STATUS = (
        ("0", "در انتظار خریدار"),
        ("1", "در انتظار تایید"),
        ("2", "انجام شده"),

    )
    CRYPTOS = [
        ("btc", "bitcoin"),
        ("ltc", "litecoin"),
        ("pax", "pax"),
        ("uni", "uniswap"),
        ("maker", "maker"),
        ("dash", "dashcoin"),
        ("eth", "ethereum"),
        ("xrp", "ripple"),
        ("bch", "bitcoincash"),
        ("eos", "eos"),
        ("ada", "kardano"),
        ("xlm", "stellar"),
        ("miota", "itota"),
        ("trx", "teron"),
        ("neo", "neo"),
        ("xmr", "monroe"),
        ("xem", "nem"),
        ("usdt", "tether"),
        ("vet", "vchain"),
        ("etc", "ethclassic"),
        ("bnb", "binance"),
        ("zec", "zicash"),
        ("qtum", "qtum"),
        ("icx", "icon"),
        ("omg", "omisgo"),
        ("lsk", "lisk"),
        ("zil", "zilika"),
        ("btg", "bitcoingold"),
        ("ae", "eternity"),
        ("doge", "doge"),
        ("kmd", "komodo"),
        ("wm", "webmoney"),
    ]
    time = models.DateTimeField(auto_now=True, auto_now_add=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cryp = models.CharField(max_length=6, null=True, choices=CRYPTOS)
    status = models.CharField(max_length=30, null=True, choices=STATUS)
    title = models.CharField(max_length=100, null=True)
    minimum = models.FloatField(max_length=10, null=True)
    fpu = models.FloatField(default=0, null=True)
    amount = models.FloatField(default=0, null=True)

    def jpublish(self):
        return jalali_converter(self.time)

    jpublish.short_description = "زمان انتشار"

    def __str__(self):
        return ((self.cryp))

    class Meta:
        verbose_name = 'معاملات بی واسطه'
        verbose_name_plural = 'معامله های بی واسطه'


class P2P_Acc2(models.Model):
    time = models.DateTimeField(auto_now=True, auto_now_add=False)
    porder = models.ForeignKey(P2P, on_delete=models.CASCADE, null=True)
    p2pbuyer = models.ForeignKey(User, on_delete=models.PROTECT, related_name='ptbuyer')
    p2pseller = models.ForeignKey(User, on_delete=models.PROTECT, related_name='ptseller')
    tsid = models.IntegerField(null=True, blank=True)
    step = models.IntegerField(default=1)
    slug = models.SlugField(unique=True, max_length=200, null=True, blank=True)
    amount = models.FloatField(max_length=10, null=True)
    rand = models.CharField(max_length=100, null=True)
    wallet = models.CharField(max_length=100, null=True)

    class Meta:
        verbose_name = 'مدیریت کاربران معامله بی واسطه'
        verbose_name_plural = 'مدیریت کاربر های معاملات بی واسظه'

    def save(self, *args, **kwargs):
        (value) = str(self.p2pseller.id) + '_' + str(self.p2pbuyer.id) + '_' + str(self.rand)
        self.slug = slugify(value)
        super(P2P_Acc2, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('account:detail_btc', args=[self.slug])
    def jpublish(self):
        return jalali_converter(self.time)

    jpublish.short_description = "زمان انتشار"


class Blog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now=True, auto_now_add=False)
    description = models.CharField(max_length=100, null=True)
    title = models.CharField(max_length=100, null=True)
    image = models.ImageField(upload_to='images')

    def jpublish(self):
        return jalali_converter(self.time)
    def __str__(self):
        return self.title
