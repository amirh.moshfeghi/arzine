from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from .models import User, Tickets, Transactions, Order, P2P, P2P_Acc, Account, UserAuthModel, UserWallet, Profile, \
	BlockedCoin, Notification, Blog

UserAdmin.fieldsets[2][1]['fields'] = (
										'is_active',
										'is_staff',
										'is_superuser',
										'special_user',
										'groups',
										'user_permissions'
									)
UserAdmin.list_display = ( 'is_special_user',)

admin.site.register(User, UserAdmin)
admin.site.register(Tickets)
admin.site.register(Transactions)
admin.site.register(Account)
admin.site.register(Order)
admin.site.register(P2P)
admin.site.register(P2P_Acc)
admin.site.register(UserWallet)
admin.site.register(Profile)
admin.site.register(UserAuthModel)
admin.site.register(BlockedCoin)
admin.site.register(Notification)
admin.site.register(Blog)