from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User


class ProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')

        super(ProfileForm, self).__init__(*args, **kwargs)

        self.fields['username'].help_text = None

        if not user.is_superuser:
            self.fields['username'].disabled = True
            self.fields['email'].disabled = True
            self.fields['special_user'].disabled = True

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name',
                  'special_user', ]


class SignupForm(UserCreationForm):
    email = forms.EmailField(max_length=200)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')


# class UserEditView(UserChangeForm):

class NormalBuyForm(forms.Form):
    amount = forms.FloatField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 0.1 '}))
    # budget = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 2 ریال'}))
    date = forms.DateField()
    fpu = forms.IntegerField()
    CHOICES = [('1', 'بله'),
               ('0', 'خیر')]

    type = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)

    def __init__(self, *args, **kwargs):
        super(NormalBuyForm, self).__init__(*args, **kwargs)

        self.fields['amount'].widget.attrs.update({'class': 'form-control', 'id': 'am1','oninput': 'myFunction1()'})
        self.fields['date'].widget.attrs.update({'class': 'form-control datetimepicker-input'})
        self.fields['fpu'].widget.attrs.update({'class': 'form-control', 'id': 'fp1','oninput': 'myFunction1()'})
        # self.fields['budget'].widget.attrs.update({'class': 'form-control'})
        self.fields['type'].widget.attrs.update(
            {'name': 'radios15', 'type': 'radio', 'class': 'radio-inline mx-5 ', 'checked': 'checked'})


class ChargeForm(forms.Form):
    amount = forms.FloatField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 2 ریال'}))
    description = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'اختیاری'}))

    def __init__(self, *args, **kwargs):
        super(ChargeForm, self).__init__(*args, **kwargs)

        self.fields['amount'].widget.attrs.update({'class': 'form-control'})
        self.fields['description'].widget.attrs.update({'class': 'form-control'})


class AuthForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : امیر'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : امیری'}))
    melli = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 431111111'}))
    card = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'شماره کارت'}))
    sheba = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'شماره شبا(بدون IR)'}))
    pic = forms.ImageField()

    mobile = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 09123456789'}))
    b_date = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 75/3/3'}))
    add = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : تهران،تهران'}))

    phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 0283333333'}))

    def __init__(self, *args, **kwargs):
        super(AuthForm, self).__init__(*args, **kwargs)

        self.fields['first_name'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['last_name'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['melli'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['card'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['sheba'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['mobile'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['phone'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['b_date'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['add'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['pic'].widget.attrs.update({'id': 'image-file2', 'class': 'valid-feedback'})


class PhoneVerify(forms.Form):
    CRYP_CHOICES = [
        ("btc", "bitcoin"),
        ("ltc", "litecoin"),
        ("pax", "pax"),
        ("uni", "uniswap"),
        ("maker", "maker"),
        ("dash", "dashcoin"),
        ("eth", "ethereum"),
        ("xrp", "ripple"),
        ("bch", "bitcoincash"),
        ("eos", "eos"),
        ("ada", "kardano"),
        ("xlm", "stellar"),
        ("miota", "itota"),
        ("trx", "teron"),
        ("neo", "neo"),
        ("xmr", "monroe"),
        ("xem", "nem"),
        ("usdt", "tether"),
        ("vet", "vchain"),
        ("etc", "ethclassic"),
        ("bnb", "binance"),
        ("zec", "zicash"),
        ("qtum", "qtum"),
        ("icx", "icon"),
        ("omg", "omisgo"),
        ("lsk", "lisk"),
        ("zil", "zilika"),
        ("btg", "bitcoingold"),
        ("ae", "eternity"),
        ("doge", "doge"),
        ("kmd", "komodo"),
        ("wm", "webmoney"),
    ]

    addr = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : uyguyitierodgho'}))
    cryp = forms.CharField(widget=forms.Select(choices=CRYP_CHOICES))






    def __init__(self, *args, **kwargs):
        super(PhoneVerify, self).__init__(*args, **kwargs)
        self.fields['cryp'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})

        self.fields['addr'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})


class FastBuyForm(forms.Form):
    amount = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 0.1 '}))
    # budget = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 2 ریال'}))
    date = forms.DateField()
    fpu = forms.IntegerField()
    CHOICES = [('1', 'بله'),
               ('0', 'خیر')]

    type = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)

    def __init__(self, *args, **kwargs):
        super(FastBuyForm, self).__init__(*args, **kwargs)

        self.fields['amount'].widget.attrs.update({'class': 'form-control', 'id': 'am2', 'onchange': 'myFunction2()'})
        self.fields['date'].widget.attrs.update({'class': 'form-control datetimepicker-input'})
        self.fields['fpu'].widget.attrs.update({'class': 'form-control', 'id': 'fp2', 'onchange': 'myFunction2()'})
        # self.fields['budget'].widget.attrs.update({'class': 'form-control'})
        self.fields['type'].widget.attrs.update(
            {'name': 'radios15', 'type': 'radio', 'class': 'radio-inline mx-5 ', 'checked': 'checked'})


class NormalSellForm(forms.Form):
    amount = forms.FloatField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 0.1 '}))
    # budget = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 2 ریال'}))
    date = forms.DateField()
    fpu = forms.FloatField()
    CHOICES = [('1', 'بله'),
               ('0', 'خیر')]

    type = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)

    def __init__(self, *args, **kwargs):
        super(NormalSellForm, self).__init__(*args, **kwargs)

        self.fields['amount'].widget.attrs.update({'class': 'form-control', 'id': 'am3', 'onchange': 'myFunction3()'})
        self.fields['date'].widget.attrs.update({'class': 'form-control datetimepicker-input'})
        self.fields['fpu'].widget.attrs.update({'class': 'form-control', 'id': 'fp3', 'onchange': 'myFunction3()'})
        # self.fields['budget'].widget.attrs.update({'class': 'form-control'})
        self.fields['type'].widget.attrs.update(
            {'name': 'radios15', 'type': 'radio', 'class': 'radio-inline mx-5 ', 'checked': 'checked'})


class FastSellForm(forms.Form):
    amount = forms.FloatField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 0.1 '}))
    # budget = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 2 ریال'}))
    date = forms.DateField()
    fpu = forms.FloatField()
    CHOICES = [('1', 'بله'),
               ('0', 'خیر')]

    type = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)

    def __init__(self, *args, **kwargs):
        super(FastSellForm, self).__init__(*args, **kwargs)

        self.fields['amount'].widget.attrs.update({'class': 'form-control', 'id': 'am4', 'onchange': 'myFunction4()'})
        self.fields['date'].widget.attrs.update({'class': 'form-control datetimepicker-input'})
        self.fields['fpu'].widget.attrs.update({'class': 'form-control', 'id': 'fp4', 'onchange': 'myFunction4()'})
        # self.fields['budget'].widget.attrs.update({'class': 'form-control'})
        self.fields['type'].widget.attrs.update(
            {'name': 'radios15', 'type': 'radio', 'class': 'radio-inline mx-5 ', 'checked': 'checked'})


class P2PCreateForm(forms.Form):
    CRYP_CHOICES = [
        ("btc", "bitcoin"),
        ("ltc", "litecoin"),
        ("pax", "pax"),
        ("uni", "uniswap"),
        ("maker", "maker"),
        ("dash", "dashcoin"),
        ("eth", "ethereum"),
        ("xrp", "ripple"),
        ("bch", "bitcoincash"),
        ("eos", "eos"),
        ("ada", "kardano"),
        ("xlm", "stellar"),
        ("miota", "itota"),
        ("trx", "teron"),
        ("neo", "neo"),
        ("xmr", "monroe"),
        ("xem", "nem"),
        ("usdt", "tether"),
        ("vet", "vchain"),
        ("etc", "ethclassic"),
        ("bnb", "binance"),
        ("zec", "zicash"),
        ("qtum", "qtum"),
        ("icx", "icon"),
        ("omg", "omisgo"),
        ("lsk", "lisk"),
        ("zil", "zilika"),
        ("btg", "bitcoingold"),
        ("ae", "eternity"),
        ("doge", "doge"),
        ("kmd", "komodo"),
        ("wm", "webmoney"),
    ]

    amount = forms.FloatField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 1'}))
    minimum = forms.FloatField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 1'}))
    cryp = forms.CharField(widget=forms.Select(choices=CRYP_CHOICES))
    fpu = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : 1'}))

    def __init__(self, *args, **kwargs):
        super(P2PCreateForm, self).__init__(*args, **kwargs)

        self.fields['amount'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['minimum'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['cryp'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})
        self.fields['fpu'].widget.attrs.update({'class': 'form-control form-control-lg form-control-solid'})


class WalletForm(forms.Form):
    add = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'مثال : yuyiyuiyui'}))

    def __init__(self, *args, **kwargs):
        super(WalletForm, self).__init__(*args, **kwargs)

        self.fields['add'].widget.attrs.update({'class': 'form-control'})
class Wverify(forms.Form):
    code = forms.CharField(widget=forms.TextInput())

    def __init__(self, *args, **kwargs):
        super(Wverify, self).__init__(*args, **kwargs)

        self.fields['code'].widget.attrs.update({'class': 'form-control'})

