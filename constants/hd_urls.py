urls = {
    # 'btc':'https://api.cryptoapis.io/v1/bc/btc/testnet/wallets/hd/BTC/addresses/generate' ,
    'btc': 'https://app.bitgo.com/api/v2/btc/wallet/5fe0e9cddcb87a0066fc635ccde9dd9f/address',
    'dash': 'https://app.bitgo.com/api/v2/dash/wallet/5fe30d01d07698002426b9de097ddc43/address',
    'bch': 'https://app.bitgo.com/api/v2/bch/wallet/5fe30bde7e3f5c006617f48bf3df7242/address',
    'btg': 'https://app.bitgo.com/api/v2/btg/wallet/5fe30c5f2edce1001d462ef39d9bcf30/address',
    'ltc': 'https://app.bitgo.com/api/v2/ltc/wallet/5fe30c26d3ec88001697a1fec7b7a100/address',
    'zec': 'https://app.bitgo.com/api/v2/zec/wallet/5fe30d41b0503e0032690536fcd8ac27/address',
    'eth': 'https://api-eu1.tatum.io/v3/ethereum/address/xpub6EBUZfHWJb8PXfmEjDofah2wBCUeqX6NzMdXTsoiHVyYdN7xTywsENAPKteapsqbyf4DtNcQECDWsNApX3KHsYmU4Re8UgikvWnDkcXdEWr/0',

    #     'dash': 'https://api.cryptoapis.io/v1/bc/dash/testnet/wallets/hd/DASH/addresses/generate',
    #     'doge' : "https://api.cryptoapis.io/v1/bc/doge/testnet/wallets/hd/DOGE/addresses/generate",
    #     'ltc'  : "https://api.cryptoapis.io/v1/bc/ltc/testnet/wallets/hd/LTC/addresses/generate",
}
